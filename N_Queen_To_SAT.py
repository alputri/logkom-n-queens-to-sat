import tkinter as tk
from math import ceil
import subprocess

window = tk.Tk()
window.title("Computational Logic Assignment - N-Queens To SAT")

background_color = "#F7EFE8"
color1 = "#EFDAB7"
color2 = "#B48966"

window.configure(bg=background_color)

dummy_img = tk.PhotoImage()
queen_photo_big = tk.PhotoImage(file='crown-32.gif')
queen_photo_medium = tk.PhotoImage(file='crown-16.gif')
queen_photo_small = tk.PhotoImage(file='crown-8.gif')
queen_icon = tk.PhotoImage(file='crown-512.gif')
window.tk.call('wm', 'iconphoto', window._w, queen_icon)

def text_error():
    tk.Label(window, text='ERROR', fg="red", bg=background_color, font='Calibri 12 bold').grid(row=3, column=0, pady="4")
    tk.Label(window, bg=background_color, text='Input is not an integer greater than 0', font='Calibri 12').grid(row=4, column=0, pady="4")

def text_display(n):
    tk.Label(window, text='N-Queens Solution', bg=background_color, font='Calibri 12 bold').grid(row=3, column=0, pady="4")

    if(n>60): #display on terminal
        boardsize = "Board Size: " + str(n) + "x" + str(n)
        tk.Label(window, text=boardsize, bg=background_color, font='Calibri 12 bold').grid(row=4, column=0)
        tk.Label(window, bg=background_color, text='Result is displayed on terminal', font='Calibri 12').grid(row=5, column=0, pady="4")
    else: #unsolvable
        result = "The problem cannot be solved for N = {}".format(n)
        tk.Label(window, text=result, bg=background_color, font='Calibri 12').grid(row=5, column=0, padx="4", pady="4")


def display_on_gui(n, grid_size):
    
    color = color1

    with open('N_Queen_To_SAT.sol', mode='r') as f:
        content = f.read().splitlines()
    f.close()

    solved = content[0]

    if solved=='UNSAT':
        text_display(n)
        return
        
    result = content[1].split()

    #text display
    tk.Label(window, text='N-Queens Solution', bg=background_color, font='Calibri 12 bold').grid(row=0, column=1, rowspan=2, columnspan=n)
    boardsize = "Board Size: " + str(n) + "x" + str(n)
    tk.Label(window, text=boardsize, bg=background_color, font='Calibri 12 bold').grid(row=2, column=1, columnspan=n)

    r = 2
    c = 1

    for i in range(n*n):

        if (i%n) == 0:
            c = 1
            r += 1

            if (n%2) == 0:
                color = color1 if color == color2 else color2

        tile = int(result[i])

        if tile < 0:
            tk.Label(window, bg=color, image=dummy_img, width=grid_size, height=grid_size, compound='center').grid(row=r, column=c)
        else:
            if n <= 20:
                photo = queen_photo_big
            elif n<= 40:
                photo = queen_photo_medium
            elif n<= 60:
                photo = queen_photo_small
            tk.Label(window, bg=color, image=photo, width=grid_size, height=grid_size, compound='center').grid(row=r, column=c)

        c += 1
        color = color1 if color == color2 else color2
      

    window.mainloop()

def display_on_terminal(n):

    text_display(n)

    with open('N_Queen_To_SAT.sol', mode='r') as f:
        content = f.read().splitlines()
    f.close()

    solved = content[0]

    if solved=='UNSAT':
        print("This problem is unsolvable for N = %d" %n)
        return

    result = content[1].split()

    r = -1
    c = 0

    for i in range(n*n):
        if (i%n) == 0:
            r += 1
            c = 0
            print('')

        tile = int(result[i])

        if tile < 0:
            print(" _ ", end="")
        else:
            print(" X ", end="")

        c += 1

    print('\n')

def main():

    #erase previous result from canvas
    for label in window.grid_slaves():
        if int(label.grid_info()["column"]) > 0:
            label.destroy()

    for label in window.grid_slaves():
        if int(label.grid_info()["row"]) > 2:
            label.destroy()

    file = open("N_Queen_To_SAT.cnf", mode='w')

    string = e.get()
    print(string)
    
    #validate if received input is an integer or not
    try: 
        n = int(string)
    except:
        text_error()
        return

    #validate if received input is less than 1 (not valid)
    if(n < 1):
        text_error()
        return
    #validate if received input is 1 (can't be computed)
    if(n == 1):
        text_display(n)
        return

    counter = (n * (n - 1) * (5 * n - 1)) / 3
    counter += n

    # file header
    file.write("c N-Queen to SAT converter\n")
    file.write("p cnf {0:d} {1:d}\n".format((n * n), int(counter)))

    get_row_clauses(n, file)
    get_col_clauses(n, file)
    get_diag_clauses(n, file)

    file.close()

    subprocess.call(['minisat', 'N_Queen_To_SAT.cnf', 'N_Queen_To_SAT.sol'])

    if n <= 20:
        display_on_gui(n, 32)
    elif n<= 40:
        display_on_gui(n, 16)
    elif n<= 60:
        display_on_gui(n, 8)
    else:
        display_on_terminal(n)


def get_row_clauses(n, file):
    # setting number of squares
    lim = n * n + 1
    # global file

    for i in range(1, lim):
        file.write("{0:d} ".format(i))
        if i % n == 0:
            file.write("0\n")
            

    # goes through all squares in the board
    for i in range(1, lim):
        row = ceil(i / n)

        for j in range(i, row * n + 1):
            if j == i:  # skips if row = column, as it is meaningless as a SAT clause
                continue
            file.write("-{0:d} -{1:d} 0\n".format(i, j))


def get_col_clauses(n, file):
    lim = n * n + 1

    for i in range(1, lim):
        # loop to print the clauses
        for j in range(i, lim, n):
            if j == i:  # skips if row = column, as it is meaningless as a SAT clause
                continue
            file.write("-{0:d} -{1:d} 0\n".format(i, j))


def get_diag_clauses(n, file):
    lim = n * n + 1

    for i in range(1, lim):
        row = ceil(i / n)
        col = i % n

        if col == 0: col = n

        # print left n right diagonal clauses
        for j in range(i, min(((n - col + row) * n + 1), lim), n + 1):
            if j == i:  # skips if row = column, as it is meaningless as a SAT clause
                continue
            file.write("-{0:d} -{1:d} 0\n".format(i, j))

    for i in range(1, lim):
        for j in range(i, lim, n - 1):
            if j == i:  # skips row = column
                continue
            elif ceil((j - (n - 1)) / n) == ceil(j / n):
                break
            file.write("-{0:d} -{1:d} 0\n".format(i, j))


tk.Label(window, text='Insert Chessboard Size', font='Helvetica 12 bold', bg=background_color).grid(row=0, column=0)

e = tk.Entry(window)
e.grid(row=1, column=0)
e.focus_set()

b = tk.Button(window, text='Process', bg=color1, command=main)
b.grid(row=2, column=0)

window.mainloop()