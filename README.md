# NQueensToSAT
Reduces the N-Queens Problem to a Boolean Satisfiability Problem using Python and MiniSat.

## Problem Description
In chess, a queen can attack horizontally, vertically, and diagonally. The N-queens problem asks:

> How can N queens be placed on an NxN chessboard so that no two of them attack each other?

## Requirements
1. Python 3  
2. MiniSat Solver
   Windows (Pre-compiled binary): http://minisat.se/downloads/MiniSat_v1.14_cygwin
   MacOS: https://github.com/danielsuo/minisat-os-x
   Linux (Pre-compiled binary): http://minisat.se/downloads/MiniSat_v1.14_linux
3. tkinter (GUI)

## How to Run
1. Install the MiniSat solver
2. Execute the python file (`N_Queen_To_SAT_Merge.py`) 

## Input Format
Enter the chessboard size in the input field. The input only processes integers greater than 0.

## Output Format
If the size is less than and equal to 60, the placement will be displayed on GUI. If the size is greater than 60, the placement will be displayed on terminal.